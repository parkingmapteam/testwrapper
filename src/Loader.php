<?php

namespace ParkingMap;

use DateTime;
use Parkingmap\Wrapper\Event\Event;

class Loader
{
    const PATH = __DIR__ . "/../assets";
    const EVENTS_BOX = Loader::PATH . "/events/boxs.json";
    const ITEMS_BOX = Loader::PATH . "/items/boxs.json";
    const ITEMS_SPOT = Loader::PATH . "/items/spots.json";
    const ITEMS_SECTION = Loader::PATH . "/items/sections.json";

    public static function loadEventBox(string $boxname): Event
    {
        $boxs = json_decode(file_get_contents(Loader::EVENTS_BOX), true);
        return new Event('box.' . $boxname, new DateTime(), $boxs[$boxname] ?? []);
    }

    public static function loadItemBox(string $boxname): Item
    {
        $boxs = json_decode(file_get_contents(Loader::ITEMS_BOX), true);
        return new Item($boxname, $boxname, $boxs[$boxname] ?? []);
    }

    public static function loadItemSpot(string $spotname): Item
    {
        $spot = json_decode(file_get_contents(Loader::ITEMS_SPOT), true);
        return new Item($spotname, $spotname, $spot[$spotname] ?? []);
    }

    public static function loadItemSection(string $sectionname): Item
    {
        $section = json_decode(file_get_contents(Loader::ITEMS_SECTION), true);
        return new Item($sectionname, $sectionname, $section[$sectionname] ?? []);
    }
}
