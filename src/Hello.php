<?php

namespace ParkingMap;


use Parkingmap\Wrapper\Event\EventInterface;
use Parkingmap\Wrapper\Event\Events;
use Parkingmap\Wrapper\Item\ItemInterface;

class Hello extends Service
{

    public function run(EventInterface $event, ItemInterface $item): Events
    {
        $item->setProperty("msg", "world");
        return new Events();
    }
};
